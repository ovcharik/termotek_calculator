module.exports = (grunt) ->

  [
    'grunt-browserify'
    'grunt-contrib-connect'
    'grunt-contrib-copy'
    'grunt-contrib-htmlmin'
    'grunt-contrib-less'
    'grunt-contrib-watch'
    'grunt-inline'
  ].forEach grunt.loadNpmTasks

  grunt.initConfig
    pkg: grunt.file.readJSON 'package.json'

    less:
      dist:
        options:
          sourceMap: true
          sourceMapURL: 'styles.css.map'
        files:
          'dist/styles.css': [ 'src/**/*.less' ]
      prod:
        files:
          'build/styles.css': [ 'src/**/*.less' ]

    browserify:
      dist:
        options:
          transform: ['coffeeify']
          browserifyOptions:
            debug: true
            extensions: '.coffee'
        files:
          'dist/main.js': ['src/main.coffee']
      prod:
        options:
          transform: ['coffeeify']
          browserifyOptions:
            extensions: '.coffee'
        files:
          'build/main.js': ['src/main.coffee']

    inline:
      prod:
        options:
          cssmin: true
          uglify: true
        src : 'build/index.html'
        dest: 'build/index.html'

    htmlmin:
      options:
        removeComments: true
        collapseWhitespace: true
      prod:
        files:
          'release/index.html': 'build/index.html'

    copy:
      dist:
        files: [
          { src: 'src/index.html', dest: 'dist/index.html' }
        ]
      prod:
        files: [
          { src: 'src/index.html', dest: 'build/index.html' }
        ]

    connect:
      server:
        options:
          debug: false
          port: 9000
          hostname: '0.0.0.0'
          livereload: true
          base: ['.', 'dist']

    watch:
      options:
        atBegin: false
        livereload: true
      configFiles:
        options:
          reload: true
          atBegin: false
        files: ['Gruntfile.coffee', 'package.json']
        tasks: ['build:dist']
      less:
        options:
          livereload: false
        files: ['src/**/*.less']
        tasks: ['less:dist']
      css:
        files: ['dist/styles.css']
        tasks: []
      html:
        files: ['src/index.html']
        tasks: ['copy:dist']
      browserify:
        files: ['src/**/*.coffee']
        tasks: ['browserify:dist']

  grunt.registerTask 'build:dist', ['less:dist', 'browserify:dist', 'copy:dist']
  grunt.registerTask 'build:prod', ['less:prod', 'browserify:prod', 'copy:prod', 'inline:prod', 'htmlmin:prod']
  grunt.registerTask('default', ['build:dist', 'connect', 'watch'])
