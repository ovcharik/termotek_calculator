$ = require 'jquery'

class Converter

  url: "//query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22USDRUB,EURRUB%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback="

  rates:
    RUB: 1
    EUR: 69.4888726

  mod:
    RUB: 1
    EUR: 1.02

  constructor: (@ready) ->
    $.ajax
      url: @url
      method: 'GET'
      dataType: 'json'
      success: (data) =>
        try
          for rate in data.query.results.rate when rate.id == "EURRUB"
            @rates['EUR'] = Number(rate.Rate)
        catch
          undefined
        @ready()
      error: ->
        @ready()

  sum: (obj = {}) ->
    sum  = (obj.RUB ? 0) * @rates.RUB * @mod.RUB
    sum += (obj.EUR ? 0) * @rates.EUR * @mod.EUR
    sum

module.exports = Converter
