_ = require('underscore')
Boxes = require('./boxes')
Collectors = require('./collectors')
Other = require('./items')

class Calculator

  constructor: (data) ->
    @boxes      = new Boxes      data.boxes
    @collectors = new Collectors data.collectors, data.areaRules, @boxes
    @other      = new Other      data.other

  calc: (areas = []) ->
    collectors = @collectors.calc areas

    contours = @collectors.getContours collectors
    boxes    = @collectors.getBoxes    collectors

    area  = _(areas).reduce ((m, a) -> m + a), 0
    other = @other.map (o) -> o.calc(area, collectors.length, contours)

    result = []
    result.push @collectors.toArray(collectors)
    result.push @boxes.toArray(boxes)
    result.push other
    _.flatten(result)

module.exports = Calculator
