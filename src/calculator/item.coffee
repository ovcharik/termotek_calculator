class Item

  constructor: (@data = {}) ->

  calc: (area = 0, collectors = 0, contours = 0) ->
    count = 1
    count = eval(@data.rule) if @data.rule
    {
      cost    : @data.cost
      count   : count
      currency: @data.currency

      title  : @data.title
      measure: @data.measure

      sortIndex: @data.sortIndex
    }

module.exports = Item
