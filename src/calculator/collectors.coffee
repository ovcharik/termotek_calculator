_ = require('underscore')
Items     = require('./items')
Collector = require('./collector')

class Collectors extends Items

  Class: Collector

  constructor: (list = {}, @areaRules = [], @boxes) ->
    super

  getById: (id = null) ->
    return unless id?
    for c in @collection
      return c if id == c.id
    return

  calc: (areas = []) ->
    result = []
    for area in areas
      for rule in @areaRules when rule.range[0] <= area <= rule.range[1]
        result.push(rule.collectors)
        break
    _.chain(result)
      .flatten()
      .map (id) => @getById(id)
      .compact()
      .value()

  getContours: (collectors = []) ->
    _(collectors).reduce ((m, c) => m + c.contours()), 0

  getBoxes: (collectors = []) ->
    _(collectors).map (c) => @boxes.getById c.box()

  toArray: (collectors = []) ->
    _.chain(collectors)
      .reduce (m, c) ->
        if m[c.id]
          m[c.id].count += 1
        else
          m[c.id] = c.calc()
        m
      , {}
      .map (c) -> c
      .value()


module.exports = Collectors
