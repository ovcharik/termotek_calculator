_ = require('underscore')
Items     = require('./items')
Box = require('./box')

class Collectors extends Items

  Class: Box

  constructor: (list = {}) ->
    super

  getById: (id = null) ->
    return unless id?
    for c in @collection
      return c if id == c.id
    return

  toArray: (collectors = []) ->
    _.chain(collectors)
      .reduce (m, c) ->
        if m[c.id]
          m[c.id].count += 1
        else
          m[c.id] = c.calc()
        m
      , {}
      .map (c) -> c
      .value()

module.exports = Collectors
