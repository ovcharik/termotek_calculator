Item = require('./item')

class Collector extends Item

  constructor: (@data = {}, @id = null) ->

  contours: ->
    @data.contours

  box: ->
    @data.box

module.exports = Collector
