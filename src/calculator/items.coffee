_ = require('underscore')
Item = require('./item')

class Items

  Class: Item

  constructor: (list = []) ->
    @collection = []
    @each list, (data, index) =>
      @collection.push new @Class(data, index)

  each: (data = null, f = ->) ->
    if typeof(data) == 'function'
      f = data
      data = null

    unless data
      data = @collection

    _(data).each f

  map: (data = null, f = ->) ->
    if typeof(data) == 'function'
      f = data
      data = null

    unless data
      data = @collection

    _(data).map f

module.exports = Items
