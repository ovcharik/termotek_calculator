_ = require 'underscore'

$ = window.$ = window.jQuery = require 'jquery'
require 'jquery-serializejson'

require './number'

Converter = require('./converter')

class FloorElement

  template: _.template $("#calc-floor").html()

  constructor: (@collection) ->
    @removed = false
    @$el = $(@template({floor: 1}))
    @$el.addClass 'calc-floor'
    @$number = @$el.find('.calc-floor-number')
    @$remove = @$el.find('.calc-floor-button-remove')

    @$remove.on 'click', => @remove()
    return @

  setNumber: (@number) ->
    @$number.text @number

  remove: ->
    @$el.remove()
    @removed = true
    @collection.updateFloors()
    return @


class FloorsElement

  constructor: (@$parent, @$add) ->
    @list = []
    @updateFloors()

    @$add.on 'click', => @append(); @updateFloors()

    return @

  updateFloors: ->
    @list = _(@list).filter (f) -> not f.removed
    @append() unless @list.length
    for f, i in @list
      f.setNumber i + 1
    return @

  append: ->
    f = new FloorElement @
    @list.push f
    @$parent.append f.$el
    return @


class ResultElement

  designWork: 50
  erectionWork: 360

  itemTemplate: _.template $("#calc-result-row").html()

  constructor: (@converter, @$el) ->
    @$list = @$el.find('.calc-result-table')
    @$sum  = @$el.find('.calc-result-sum')

  show: (areas, data) ->
    sumAreas = []
    for key, value of areas
      sumAreas = sumAreas.concat(value)
    sumAreas = _(sumAreas).reduce ((m, i) -> m + i), 0

    @$el.show()
    @$list.html ""

    @updateItems(data.sum)
    @updateList(data.items)
    @updateDesign(sumAreas)
    @updateErection(sumAreas)
    @updateResult(sumAreas, data.sum)
    return @

  updateItems: (sum = {}) ->
    @$list.append @itemTemplate
      cl: 'main'
      count: Math.round(@converter.sum(sum))
      measure: 'руб.'
      title: 'Стоимость оборудования и комплектующих'

  updateList: (items = []) ->
    for i in items
      i.cl = 'item'
      @$list.append @itemTemplate(i)

  updateDesign: (area = 0) ->
    @$list.append @itemTemplate
      cl: 'main'
      count: @designWork * area
      measure: 'руб.'
      title: 'Стоимость проектных работ'

  updateErection: (area = 0) ->
    @$list.append @itemTemplate
      cl: 'main'
      count: @erectionWork * area
      measure: 'руб.'
      title: 'Стоимость монтажных работ'

  updateResult: (area = 0, sum = {}) ->
    r = 0
    r += @designWork * area
    r += @erectionWork * area
    r += Math.round(@converter.sum(sum))

    @$list.append @itemTemplate
      cl: 'main result'
      count: r
      measure: 'руб.'
      title: 'Общая стоимость системы отопления "Водяной теплый пол"'


calc = new (require('./calc'))
conv = new Converter =>

  $form     = $('#calc-form')
  $floors   = $('#calc-floors')
  $addFloor = $('#calc-append-floor')
  $result   = $('#calc-result')

  floors = new FloorsElement $floors, $addFloor
  result = new ResultElement conv, $result

  $form.on 'submit', (event) =>
    event.preventDefault()
    formData = $form.serializeJSON()
    data = {}

    for area, i in formData.areas when Number(area)
      area = Number(area)
      data[formData.classes[i]] ?= []
      data[formData.classes[i]].push area

    result.show data, calc.calc(data)

$ =>
  $modal    = $('#calc-modal')
  $close    = $('#calc-close')
  $body     = $('body')

  open = ->
    $modal.fadeIn()
    $body.css 'overflow': 'hidden'

  close = ->
    $modal.fadeOut()
    $body.css 'overflow': ''

  $close.on 'click', close
  $body.on 'click', '*[href="#calc"]', open

  window.calcOpen  = open
  window.calcClose = close
