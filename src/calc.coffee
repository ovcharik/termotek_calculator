_ = require 'underscore'
Calculator = require './calculator'

data =
  standart : require './data/standart'
  premium  : require './data/premium'

class Calc

  constructor: ->
    @standart = new Calculator data.standart
    @premium  = new Calculator data.premium

  calc: (areas = {}) ->

    list = []
    list = list.concat @standart.calc areas.standart if areas.standart
    list = list.concat @premium.calc  areas.premium  if areas.premium

    list = _.chain(list)
      .reduce (m, i) =>
        unless m[i.title]
          m[i.title] = i
        else
          m[i.title].count += i.count
        m
      , {}
      .map (i) => i
      .sortBy (i) => i.sortIndex
      .value()

    sum = @toSum(list)
    {
      items: list
      sum  : sum
    }

  toSum: (result) ->
    _.chain(result)
      .groupBy (i) -> i.currency
      .map (items, c) ->
        [
          c
          _(items).reduce ((sum, i) -> sum += i.count * i.cost), 0
        ]
      .object()
      .value()

module.exports = Calc
